import os
import os.path as osp
import shutil
import sqlite3

try:
    os.unlink('copy_plan.db')
except FileNotFoundError:
    pass
con = sqlite3.connect('copy_plan.db')
cur = con.cursor()
con.row_factory = sqlite3.Row

cur.execute("CREATE TABLE IF NOT EXISTS top_dirs (dir_name primary key, source)")
cur.execute("""
    CREATE TABLE IF NOT EXISTS files ( 
      rel_path, dir_name REFERENCES top_dirs(dir_name), status
      )
""")

def fmt_bytes(n: int) -> str:
    for unit in ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB']:
        if n < 1024:
            break
        n /= 1024

    return f'{n:.01f} {unit}'

n_dirs = n_files = n_bytes = 0

SOURCE_DIRS = ['/home/takluyver/Pictures']
DEST_ROOT = '/home/takluyver/Code/kfc/eg_dest'

top_dir_names = set()

print("Scanning...")
for source_dir in SOURCE_DIRS:
    dir_name = dir_name_orig = os.path.basename(source_dir)
    counter = 0
    while dir_name in top_dir_names:
        counter += 1
        dir_name = f"{dir_name_orig} ({counter})"
    top_dir_names.add(dir_name)
    
    cur.execute("INSERT INTO top_dirs VALUES(?, ?)", (dir_name, source_dir))

    for root, dirs, files in os.walk(source_dir):
        n_dirs += 1
        dir_rel_path = osp.relpath(root, source_dir)
        for fname in sorted(files):
            fpath = osp.join(root, fname)
            n_bytes += os.stat(fpath).st_size
            cur.execute("INSERT INTO files VALUES(?, ?, 0)",
                (osp.relpath(fpath, source_dir), dir_name))

        n_files += len(files)
        print(f"\r{n_files} files ({fmt_bytes(n_bytes)}) "
              f"in {n_dirs} directories", end='')
        dirs.sort()

print()
con.commit()

REALLY = False

last_dir_created = None

n_files_copied = n_bytes_copied = 0

for rowid, rel_path, dir_name, source_dir in cur.execute("SELECT files.rowid, rel_path, dir_name, source FROM files JOIN top_dirs USING (dir_name)"):
    source_path = osp.join(source_dir, rel_path)
    dst_path = osp.join(DEST_ROOT, dir_name, rel_path)
    
    dst_dir = os.path.dirname(dst_path)
    if dst_dir != last_dir_created:
        if REALLY:
            os.makedirs(dst_dir, exist_ok=True)
        last_dir_created = dst_dir
    
    if REALLY:
        shutil.copy2(source_path, dst_path)
    n_files_copied += 1
    n_bytes_copied += os.stat(source_path).st_size
    print(f"\rCopied {n_files_copied}/{n_files} files ({fmt_bytes(n_bytes_copied)}/{fmt_bytes(n_bytes)})", end='')
    
print()


import errno
import os
import os.path as osp
import re
import shutil
import sys
import traceback
from pathlib import Path
from threading import Event

from PyQt5 import QtCore

REALLY = True  # Set to False for testing

# These characters can't be used on Windows drives (FAT/NTFS)
# https://stackoverflow.com/questions/1976007/what-characters-are-forbidden-in-windows-and-linux-directory-names
tricky_chars_re = re.compile(r'[<>:"\\|?*]')


def has_tricky_chars(path):
    return bool(tricky_chars_re.search(path))


def files_sort_key(path):
    # Sort files with tricky characters after those without, to simplify
    # checking for name collisions
    return has_tricky_chars(path), path


def fix_tricky_chars(path):
    return tricky_chars_re.sub('_', path)


def get_new_name(orig_name, used):
    name = orig_name
    counter = 0
    while name in used:
        counter += 1
        orig_stem, orig_ext = os.path.splitext(orig_name)
        name = f'{orig_stem} ({counter}){orig_ext}'
    return name


def find_copies(source_dir: Path, dest_dir: Path):
    """Generate pairs of source_file, dest_file paths to be copied

    This recurses into subfolders, and renames files and folders if they use
    characters that aren't allowed on FAT/NTFS. It doesn't take any account
    of files that already exist at the destination, however.
    """
    src_dirnames = []
    src_filenames = []
    for dirent in os.scandir(source_dir):
        if dirent.is_dir():
            src_dirnames.append(dirent.name)
        else:
            src_filenames.append(dirent.name)

    # Prepare directory names first, so if a renamed file clashes with a
    # directory, the file gets a suffix like ' (1)' rather than the directory.
    dst_dirnames = []
    for src_dirname in sorted(src_dirnames, key=files_sort_key):
        dst_dirname = get_new_name(fix_tricky_chars(src_dirname), dst_dirnames)
        dst_dirnames.append(dst_dirname)

    dst_all_names = set(dst_dirnames)
    for src_filename in sorted(src_filenames, key=files_sort_key):
        dst_filename = get_new_name(fix_tricky_chars(src_filename), dst_all_names)
        dst_all_names.add(dst_filename)

        yield (source_dir / src_filename), (dest_dir / dst_filename)

    for src_dirname, dst_dirname in zip(src_dirnames, dst_dirnames):
        yield from find_copies(source_dir / src_dirname, dest_dir / dst_dirname)


class Worker(QtCore.QObject):
    scan_result = QtCore.pyqtSignal(object)
    copy_progress = QtCore.pyqtSignal(object)
    copy_paused = QtCore.pyqtSignal(str)
    copy_failed = QtCore.pyqtSignal(str)
    copy_completed = QtCore.pyqtSignal()

    def __init__(self):
        super().__init__()
        self.continue_evt = Event()
        self.abort = False

    def scan(self, path):
        n_files = n_bytes = n_dirs = 0

        for root, dirs, files in os.walk(path):
            n_dirs += 1
            for fname in files:
                fpath = osp.join(root, fname)
                n_bytes += os.stat(fpath, follow_symlinks=False).st_size

            n_files += len(files)

        print(f"B: found {n_files} files", file=sys.stderr)
        self.scan_result.emit({
            'path': path, 'files': n_files, 'bytes': n_bytes, 'dirs': n_dirs
        })

    def pause_copying(self):
        self.continue_evt.clear()

    def resume_copying(self):
        self.continue_evt.set()

    def abort_copying(self):
        self.abort = True
        self.continue_evt.set()

    def run_copy(self, sources, dest_dir):
        self.continue_evt.set()
        self.abort = False

        print("B: Scanning...", file=sys.stderr)
        top_dirs = {}
        n_files = n_bytes = n_dirs = 0
        for source_dir in sources:
            source_dir = Path(source_dir)
            dir_name = get_new_name(fix_tricky_chars(source_dir.name), top_dirs)
            top_dirs[dir_name] = source_dir

            for root, dirs, files in os.walk(source_dir):
                n_dirs += 1
                for fname in sorted(files):
                    fpath = osp.join(root, fname)
                    n_bytes += os.stat(fpath).st_size

                n_files += len(files)

        print("B: copying...", file=sys.stderr)

        last_dir_created = None
        n_files_copied = n_bytes_copied = 0

        def send_progress():
            self.copy_progress.emit({
                'n_files_copied': n_files_copied,
                'n_files': n_files,
                'n_bytes_copied': n_bytes_copied,
                'n_bytes': n_bytes,
            })

        def files_to_copy():
            for dir_name, source_dir in top_dirs.items():
                dest_dir2 = Path(dest_dir, dir_name)
                yield from find_copies(source_dir, dest_dir2)

        def copy_one_file(source_path, dst_path):
            nonlocal last_dir_created

            dst_dir = os.path.dirname(dst_path)
            if dst_dir != last_dir_created:
                if REALLY:
                    os.makedirs(dst_dir, exist_ok=True)
                last_dir_created = dst_dir

            if REALLY:
                shutil.copy2(source_path, dst_path)

        try:
            for source_path, dst_path in files_to_copy():
                if not self.continue_evt.is_set():
                    send_progress()
                    self.continue_evt.wait()
                if self.abort:
                    return

                while True:
                    try:
                        copy_one_file(source_path, dst_path)
                        break
                    except OSError as e:
                        if e.errno != errno.ENOSPC:
                            raise
                        try:
                            os.unlink(dst_path)
                        except FileNotFoundError:
                            pass
                        self.copy_paused.emit(
                            "Out of space in destination - free "
                            "up some space before resuming"
                        )
                        self.continue_evt.clear()
                        self.continue_evt.wait()

                n_files_copied += 1
                n_bytes_copied += os.stat(source_path).st_size

                if n_files_copied % 20 == 0:
                    send_progress()
        except Exception as e:
            self.copy_failed.emit(str(e))
            traceback.print_exc()
        else:
            send_progress()
            self.copy_completed.emit()

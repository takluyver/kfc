import os
import sys

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog

from backend import Worker
from ui import Ui_MainWindow

def fmt_bytes(n: int) -> str:
    for unit in ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB']:
        if n < 1024:
            break
        n /= 1024

    return f'{n:.01f} {unit}'


class MainWindow(QtWidgets.QMainWindow):
    source_folder_added = QtCore.pyqtSignal(str)
    copy_launched = QtCore.pyqtSignal(object, str)
    copy_in_progress_changed = QtCore.pyqtSignal(bool)

    def __init__(self):
        super().__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        
        self.ui.add_source_button.clicked.connect(self.add_source_dir)
        self.ui.remove_source_button.clicked.connect(self.remove_source_dir)
        self.ui.dst_button.clicked.connect(self.select_destination)
        self.ui.copy_button.clicked.connect(self.start_copy)
        self.ui.pause_button.clicked.connect(self.pause_clicked)
        self.ui.cancel_button.clicked.connect(self.cancel_clicked)

        self.copy_in_progress_changed.connect(self.ui.copy_button.setDisabled)
        self.copy_in_progress_changed.connect(self.ui.pause_button.setEnabled)
        self.copy_in_progress_changed.connect(self.ui.cancel_button.setEnabled)
        self.copy_in_progress_changed.connect(self.ui.sources_group.setDisabled)
        self.copy_in_progress_changed.connect(self.ui.dst_group.setDisabled)

        self.sources_info = {}

        self.worker_thread = QtCore.QThread()
        self.worker_thread.finished.connect(self.worker_thread.deleteLater)
        self.worker_thread.start()

        self.worker = Worker()
        self.worker.moveToThread(self.worker_thread)
        self.source_folder_added.connect(self.worker.scan)
        self.copy_launched.connect(self.worker.run_copy)
        self.worker.scan_result.connect(self.on_scan_reply)
        self.worker.copy_progress.connect(self.show_progress)
        self.worker.copy_completed.connect(self.copy_completed)
        self.worker.copy_paused.connect(self.copy_paused)
        self.worker.copy_failed.connect(self.copy_failed)

    def closeEvent(self, event):
        self.worker_thread.quit()

    def add_source_dir(self):
        path = QFileDialog.getExistingDirectory(self, "Select source directory")
        if not path:
            return
        self.ui.sources_list.addItem(path)
        self.ui.remove_source_button.setEnabled(True)
        self.ui.copy_button.setEnabled(self.start_possible())
        self.source_folder_added.emit(path)

    def on_scan_reply(self, d):
        self.sources_info[d.pop('path')] = d
        self.show_source_summary()

    def show_source_summary(self):
        nbytes = nfiles = 0
        for d in self.sources_info.values():
            nbytes += d['bytes']
            nfiles += d['files']

        self.ui.statusbar.showMessage(f"To copy: {nfiles} files ({fmt_bytes(nbytes)})")

    def remove_source_dir(self):
        sl = self.ui.sources_list
        item = sl.takeItem(sl.currentRow())
        self.ui.remove_source_button.setEnabled(sl.count() > 0)
        self.ui.copy_button.setEnabled(self.start_possible())
        self.sources_info.pop(item.text(), None)
        self.show_source_summary()

    def select_destination(self):
        path = QFileDialog.getExistingDirectory(self, "Select destination directory")
        if path:
            self.ui.dst_line_edit.setText(path)
        self.check_destination()
        self.ui.copy_button.setEnabled(self.start_possible())

    def check_destination(self):
        path = self.ui.dst_line_edit.text()
        if path == '':
            self.ui.dst_nonempty_label.clear()
            self.ui.dst_freespace_label.clear()
            return

        if os.listdir(path) == []:
            self.ui.dst_nonempty_label.clear()
        else:
            self.ui.dst_nonempty_label.setText(
                "This folder is not empty; files may be overwritten"
            )
        fsstat = os.statvfs(path)
        bytes_avail = fsstat.f_frsize * fsstat.f_bavail
        self.ui.dst_freespace_label.setText(f"Free space: {fmt_bytes(bytes_avail)}")

    def start_possible(self):
        return self.ui.dst_line_edit.text() != '' and self.ui.sources_list.count() > 0

    def start_copy(self):
        sl = self.ui.sources_list
        sources = [sl.item(r).text() for r in range(sl.count())]
        dest_dir = self.ui.dst_line_edit.text()
        self.copy_launched.emit(sources, dest_dir)

        self.ui.progress_bar.setValue(0)
        self.copy_in_progress_changed.emit(True)

    def pause_clicked(self, checked):
        print("F: pause clicked, checked", checked)
        if checked:
            self.worker.pause_copying()
        else:
            self.worker.resume_copying()

    def cancel_clicked(self):
        self.worker.abort_copying()
        self.check_destination()
        self.ui.progress_bar.setValue(0)
        self.ui.pause_button.setChecked(False)
        self.ui.statusbar.showMessage(
            "Copying cancelled, but some files may already have been copied"
        )
        self.copy_in_progress_changed.emit(False)

    def show_progress(self, d):
        n_files_copied, n_files = d.get('n_files_copied'), d.get('n_files')
        n_bytes_copied, n_bytes = d.get('n_bytes_copied'), d.get('n_bytes')
        self.ui.statusbar.showMessage(
            f"Copied {n_files_copied} / {n_files} files — "
            f"{fmt_bytes(n_bytes_copied)} / {fmt_bytes(n_bytes)}"
        )
        self.ui.progress_bar.setValue(int(100 * n_bytes_copied / n_bytes))

    def copy_paused(self, msg):
        self.ui.pause_button.setChecked(True)
        self.ui.statusbar.showMessage(msg)

    def copy_completed(self):
        self.check_destination()
        self.copy_in_progress_changed.emit(False)

    def copy_failed(self, msg):
        self.ui.statusbar.showMessage(f"Copying failed: {msg}")
        self.check_destination()
        self.copy_in_progress_changed.emit(False)

def main():
    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()

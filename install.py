import venv
from pathlib import Path
from subprocess import run

this_dir = Path(__file__).resolve().parent
env_dir = this_dir / 'env'
env_py = env_dir / 'bin' / 'python'
venv.create(env_dir, with_pip=True)

run([str(env_py), '-m', 'pip', 'install', 'PyQt5'])

desktop_content = (this_dir / 'kfc.desktop').read_text().format(
    python=env_py,
    frontend_py=this_dir / 'frontend.py'
)

Path('~/.local/share/applications/kfc.desktop').expanduser().write_text(desktop_content)
